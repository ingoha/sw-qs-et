#include "../include/Sortierung.h"

Sortierung::Sortierung()
{
}

/**
 * von https://de.wikibooks.org/wiki/Algorithmen_und_Datenstrukturen_in_C/_Bubblesort
 */
void Sortierung::bubbleSort(string& s)
{
    int i, j;
    for (i = 0; i < s.size() -1; ++i)
    {

        for (j = 0; j < s.size() - i - 1; ++j)
        {
            if (s[j] > s[j + 1])
            {
                int tmp = s[j];
                s[j] = s[j + 1];
                s[j + 1] = tmp;
            }
        }
    }
}

/**
 * von https://de.wikibooks.org/wiki/Algorithmen_und_Datenstrukturen_in_C/_Quicksort
 */
void Sortierung::quickSort(string& s)
{
    quickSort(s, 0, s.size() - 1);
}

void Sortierung::quickSort(string& s, int start, int end)
{
    int ptr;
    int split;
    if (end - start <= 1)
        return;
    ptr = start;
    split = start + 1;
    while (++ptr != end)
    {
        if (s[ptr] < s[start])
        {
            char tmp = s[ptr];
            s[ptr] = s[split];
            s[split] = tmp;
            ++split;
        }
    }
    char tmp = s[start];
    s[start] = s[split - 1];
    s[split - 1] = tmp;
    quickSort(s, start, split - 1);
    quickSort(s, split, end);
}
