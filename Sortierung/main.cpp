#include <iostream>
#include <cstdio>

#include "include/Sortierung.h"

using namespace std;

int main()
{
    string hello = "Hello world!";

    printf("%s\n", hello.c_str());
    Sortierung s;
    s.quickSort(hello);
    printf("%s\n", hello.c_str());


    return 0;
}
