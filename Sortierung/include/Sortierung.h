#ifndef SORTIERUNG_H
#define SORTIERUNG_H

#include <string>
using namespace std;

class Sortierung
{
public:
    Sortierung();

    void bubbleSort(string& s);
    void quickSort(string& s);
protected:
private:
    void quickSort(string& s, int start, int end);
};

#endif // SORTIERUNG_H
